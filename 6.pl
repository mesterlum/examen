#!/usr/bin/perl
use strict;
use warnings;
package Six;


sub new 
{
    my $self = {};
    $self->{name} = "Default name";
    bless $self, "Six";
    return $self;
}

sub setName 
{
    my $self = shift;
    $self->{name} = shift;
}

sub getName 
{
    my $self = shift;
    return $self->{name}
}

my $b = new Six();
$b->setName("Eduardo");

print $b->getName() . "\n";

