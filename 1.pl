#!/usr/bin/perl
use strict;
use warnings;

my $filename = 'grid.txt';
open(my $file, '>', $filename) or die "Could not open file '$filename' $!";

for (my $i = 1; $i <= 100; $i++) {
    print $file "$i ";
    if ($i % 10 eq 0) {
        print $file "\n"
    }
} 

close $file;