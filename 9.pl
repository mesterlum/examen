#!/usr/bin/perl
use strict;
use warnings;

require HTTP::Request;
require LWP::UserAgent;
my $request = HTTP::Request->new('GET' => 'https://www.perlmonks.org/bare/?node_id=113938');

my $ua = LWP::UserAgent->new;
my $response = $ua->request($request);

my $text_response = $response->content;
$text_response =~ s/<\/*.*>//g;

print $text_response . "\n";