#!/usr/bin/perl
use strict;
use warnings;

my $word1 = "Hola";
my $word2 = "Mundo";
my $string = "Mundo Hola";
$string =~ s/$word1/$word2/o;

print $string . "\n";