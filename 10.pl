#!/usr/bin/perl
 
use DBI;
use strict;

use Data::Dumper qw(Dumper);  
 
my $driver   = "SQLite";
my $db_name = "db.db";
my $dbd = "DBI:$driver:dbname=$db_name";

my $dbh = DBI->connect($dbd, undef, undef, { RaiseError => 1 })
                      or die $DBI::errstr;
print STDERR "Conexion con la BD\n";


my $sth = $dbh->prepare("SELECT * from Employees");
$sth->execute();

my @allData;
while(my @row = $sth->fetchrow_array()) {
      my $hash = {};
      $hash->{id} = $row[0];
      $hash->{firstName} = $row[1];
      $hash->{lastName} = $row[2];
      push @allData, $hash;
}


print Dumper(@allData)



