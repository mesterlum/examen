#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper qw(Dumper);  



my $text = <<TEXT_BLOCK;

Hello /name/, how are you?

Your birthday is on /month///date///year/.

TEXT_BLOCK

my $hash =
{

name => 'John Doe',

month => 12,

date => 5,

year => 2005,

};

foreach my $key (keys %{$hash}) {
    $text =~ s/\/$key\//$hash->{$key}/g;
}
print $text;




