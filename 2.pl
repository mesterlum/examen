my $count = 0;

my %data = (Joe => 1, Mary => 2, Ed => 3, Sam => 4);

print ">> PASS ONE\n\n";

while (($k, $v) = each %data) {
print pack("A20A*", $k, $v), "\n";

last if (++$count == 1);

}

print "\n>> PASS TWO\n\n";
$count = 0;
while (($k, $v) = each %data) {

print pack("A20A*", $k, $v), "\n";
last if (++$count == 2);

}